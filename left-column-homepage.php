<?php 
	require_once('/sites/soda.nl/www/wp-config.php');

	if ( current_user_can('publish_posts') ) {
		
		if(isset($_POST['soda_linker_kolom_id'])){
			update_post_meta(602, 'soda_left_column_type', $_POST['soda_left_column_type']);
			update_post_meta(602, 'soda_left_column_img', $_POST['soda_left_column_img']);
			update_post_meta(602, 'soda_left_column_img_2', $_POST['soda_left_column_img_2']);
			update_post_meta(602, 'soda_left_column_txt_nl', $_POST['soda_left_column_txt_nl']);
			update_post_meta(602, 'soda_left_column_txt_en', $_POST['soda_left_column_txt_en']);
			update_post_meta(602, 'soda_left_column_txt_nl_2', $_POST['soda_left_column_txt_nl_2']);
			update_post_meta(602, 'soda_left_column_txt_en_2', $_POST['soda_left_column_txt_en_2']);
			update_post_meta(602, 'soda_left_column_img_big', $_POST['soda_left_column_img_big']);
		}
		
		$linker_kolom_home = get_post(602);
		$soda_linker_kolom_id = $linker_kolom_home->ID;
		$soda_left_column_type = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_type', true );
		$soda_left_column_img = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img', true );
		$soda_left_column_img_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img_2', true );
		$soda_left_column_txt_nl = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_nl', true );
		$soda_left_column_txt_en = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_en', true );
		$soda_left_column_txt_nl_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_nl_2', true );
		$soda_left_column_txt_en_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_en_2', true );
		$soda_left_column_img_big = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img_big', true );
?>

<div class="admin-anchors-container">
	<div class="anchor selected">
		<div class="admin-linker-kolom-container">
			<form method="POST" name="rkhp" class="admin-linker-kolom">
				<input type="hidden" name="soda_linker_kolom_id" value="602">
			<?php
				$quote_img_class = '';
				$img_class = '';
				$small_img_class = '';
				$big_img_class = '';
				if($soda_left_column_type=='quote'){
					$quote_img_class = 'hide';
				}
				if($soda_left_column_type!='image'&&$soda_left_column_type!='small-image'&&$soda_left_column_type!='small-image-quote'&&$soda_left_column_type!='image-left-column'){
					$img_class = 'hide';
				}
				if($soda_left_column_type=='image'){
					$img_norm_class = 'hide';
				}
				if($soda_left_column_type=='small-image'&&$soda_left_column_type=='small-image-quote'){
					$img_small_class = 'hide';
				}
				if($soda_left_column_type=='image-left-column'){
					$img_big_class = 'hide';
				}
				if($soda_left_column_type=='none'){
					$quote_img_class = 'hide';
					$img_class = 'hide';
					$img_small_class = 'hide';
					$img_norm_class = 'hide';
					$img_big_class = 'hide';
				}
	
			?>
				<fieldset>
					<legend>Left column type</legend>
					<select class="left-column-type soda_left_column_type" name="soda_left_column_type">
						<?php echo '<option value="'.$soda_left_column_type.'" selected>'.ucfirst($soda_left_column_type).'</option>';  ?>
						<option value="none">No left column</option>
						<option value="quote">Quote</option>
						<option value="image">Image</option>
						<option value="small-image">Small image</option>
						<!--<option value="small-image-quote">Small image &amp; Quote</option>-->
						<option value="image-left-column">Image in left column</option>
					</select>
				</fieldset>
				<fieldset class="left-column-images <?php echo $img_class; ?>">
					<legend>Left column image</legend>
					<input type="hidden" class="soda_left_column_img" name="soda_left_column_img" value="<?php echo $soda_left_column_img; ?>">
					<input type="hidden" class="soda_left_column_img_2" name="soda_left_column_img_2" value="<?php echo $soda_left_column_img_2; ?>">
					<input type="hidden" class="soda_left_column_img_big" name="soda_left_column_img_big" value="<?php echo $soda_left_column_img_big; ?>">
					<div class="thumb-container"> 
						<?php
							echo wp_get_attachment_image( $soda_left_column_img, 'soda_small' );
							echo wp_get_attachment_image( $soda_left_column_img_2, 'soda_small' );
							echo wp_get_attachment_image( $soda_left_column_img_big, 'soda_small' );
						?>
					</div>
					<div class="upload-image-buttons">
						<input type="button" value="Upload image 1" data-type="left-column" data-dest="soda_left_column_img" class="left_column_img <?php echo $img_big_class; ?>">
						<input type="button" value="Upload image 2" data-type="left-column" data-dest="soda_left_column_img_2" class="left_column_img_2 <?php echo $img_norm_class; ?><?php echo $img_big_class; ?>">
						<input type="button" value="Upload big image" data-type="left-column" data-dest="soda_left_column_img_big" class="left_column_img_big <?php echo $img_norm_class; ?><?php echo $img_small_class; ?>">
					</div>
				</fieldset>
				<fieldset class="left-column-text <?php echo $img_big_class; ?>">
					<legend>Left column text</legend>
					<textarea name="soda_left_column_txt_nl" class="soda_left_column_txt_nl soda-admin-translatable-field dutch textarea" lang="nl" contenteditable="true"><?php echo $soda_left_column_txt_nl; ?></textarea>
					<textarea name="soda_left_column_txt_en" class="soda_left_column_txt_en soda-admin-translatable-field english textarea" lang="en" contenteditable="true"><?php echo $soda_left_column_txt_en; ?></textarea>
					<div class="second-txt-field <?php echo $small_img_class; ?>">
						<textarea name="soda_left_column_txt_nl_2" class="soda_left_column_txt_nl_2 soda-admin-translatable-field dutch textarea" lang="nl" contenteditable="true"><?php echo $soda_left_column_txt_nl_2; ?></textarea>
						<textarea name="soda_left_column_txt_en_2" class="soda_left_column_txt_en_2 soda-admin-translatable-field english textarea" lang="en" contenteditable="true"><?php echo $soda_left_column_txt_en_2; ?></textarea>
					</div>
				</fieldset>
			    <input type="submit" value="Save" class="button button-primary button-large">
			</form>
		</div>
	</div>
</div>
<?php
	}
?>