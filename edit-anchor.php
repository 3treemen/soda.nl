<?php 
require_once('/sites/soda.nl/www/wp-config.php');

if ( current_user_can('publish_posts') ) {
		
	if(isset($_POST['id'])){
		
		$id = $_POST['id'];
		$arrs = $_POST['anchors'];
				
		$update_post = array(
			'ID'           => $id,
			'post_title'   => '[:nl]'.$arrs[1].'[:en]'.$arrs[2].'[:]',
			'post_content' => '',
		);

		wp_update_post( $update_post );
		
		$count = $arrs[0];
		unset($arrs[0],$arrs[1],$arrs[2]);

		if($update_post) {
			update_post_meta($id, 'soda_anchor_items_count', $count);
		
			$arr_keys = array('soda_anchor_img','soda_anchor_img_2','soda_anchor_img_size','soda_anchor_img_align','soda_anchor_img_txt_type','soda_anchor_img_txt_align','soda_anchor_img_txt_nl','soda_anchor_img_txt_en','soda_anchor_img_txt_nl_2','soda_anchor_img_txt_en_2','soda_anchor_video','soda_anchor_video_embed','soda_anchor_video_img','soda_anchor_video_txt_nl','soda_anchor_video_txt_en','soda_anchor_order');
			
			$arr_values = array();
			
			foreach($arrs as $key=>$value){
				$x = array();
				foreach($value as $val){
					$x[] = $val;
				}
				$arr_values[$arr_keys[$key-3]] = $x;
			}
			foreach($arr_values as $key=>$value){
				//echo $key;
				//print_r($value);
				update_post_meta($id, $key, $value);
			}
		}else{
			
			echo 'Could not update post';
			
		}

		echo $id;

	}else{
		
		echo 'Invalid ID';
		
	}

}else{
	
	echo 'User is not allowed to do this!';
	
}
?>
