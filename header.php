<?php
	require_once(dirname(__FILE__).'/classes/soda_walker_nav_menu.php');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
		
	<!-- Meta -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="description" content="<?php bloginfo('description'); ?>" />
	
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/images/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	
	<meta property="og:site_name" content="soda.nl"/>
	<meta property="og:title" content="<?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?>"/>
	<meta property="og:type" content="website"/>
	<meta name="apple-mobile-web-app-title" content="soda.nl" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
		
	<!-- CSS + jQuery + JavaScript -->
	<!--
	/* @license
	 * MyFonts Webfont Build ID 2652650, 2013-09-27T01:42:22-0400
	 * 
	 * The fonts listed in this notice are subject to the End User License
	 * Agreement(s) entered into by the website owner. All other parties are 
	 * explicitly restricted from using the Licensed Webfonts(s).
	 * 
	 * You may obtain a valid license at the URLs below.
	 * 
	 * Webfont: ITC Franklin Gothic Std Book by ITC
	 * URL: http://www.myfonts.com/fonts/itc/franklin-gothic/std-book/
	 * Copyright: Copyright &#x00A9; 1986, 1992, 1995, 2002 Adobe Systems Incorporated.  All
	 * Rights Reserved.
	 * 
	 * Webfont: ITC Franklin Gothic Std Demi by ITC
	 * URL: http://www.myfonts.com/fonts/itc/franklin-gothic/std-demi/
	 * Copyright: Copyright &#x00A9; 1986, 1992, 1995, 2001 Adobe Systems Incorporated.  All
	 * Rights Reserved.
	 * 
	 * 
	 * License: http://www.myfonts.com/viewlicense?type=web&buildid=2652650
	 * Licensed pageviews: 250,000
	 * 
	 * © 2013 MyFonts Inc
	*/

	-->
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/MyFontsWebfontsKit.css">
	<?php wp_head(); ?>
	
	<noscript>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/noscript.css" media="screen,projection,handheld" />
	</noscript>
	<!--
	Development by Joris Lindhout: http://www.grok-projects.com
	-->
</head>
<body <?php body_class(); ?>>	
	<!-- Content Wrapper -->
	<div id="content-wrapper">
    
		<!-- Header -->
		<header>
			<div class="logo">
				<h1><a href="/"><?php bloginfo('name'); ?></a></h1>
			</div>
			<!-- Main menu -->
			<div id="main_menu">
				<?php wp_nav_menu( array( 'theme_location' => 'menu', 'container'=> 'nav', 'container_id' => 'nav', 'items_wrap' => '<a class="mobile-nav" href="#"><span></span><span></span><span></span></a><ul class="%2$s">%3$s</ul>', 'walker' => new soda_walker_nav_menu  ) ); ?>
			</div>
			<div class="language-chooser-small">
				<?php echo qtranxf_generateLanguageSelectCode('text','small-screen'); ?>
			</div>
			<!-- /Main menu -->
		</header>
		<!-- /Header -->
	
		<!-- Wrapper -->
		<div class="wrapper">
			<div class="language-chooser-large">
				<?php echo qtranxf_generateLanguageSelectCode('text','large-screen'); ?>
			</div>
