<?php /* Template Name: Projecten */ ?>
<?php get_header(); ?>

<div id="breadcrumbs" class="hide">
	<?php echo qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage('[:nl]projecten[:en]projects[:]'); ?>
	/
	<?php
	$cats = get_the_category();
	foreach( $cats as $c ){
		echo strtolower($c->name);
	}
	?>
	/
	<?php the_title(); ?>
</div>

<!-- section -->
<section role="main">

	<?php
	
	$lng = qtranxf_getLanguage();
			
		$anchors = get_post_meta( get_the_ID(), 'soda_anchor_id', true );
				
		if ( $anchors ) {
			foreach ( $anchors as $anchor ) {
				
				$count = get_post_meta( $anchor, 'soda_anchor_items_count', true );
				
				$soda_anchor_img = get_post_meta( $anchor, 'soda_anchor_img', true );
				$soda_anchor_img_2 = get_post_meta( $anchor, 'soda_anchor_img_2', true );
				$soda_anchor_img_size = get_post_meta( $anchor, 'soda_anchor_img_size', true );
				$soda_anchor_img_align = get_post_meta( $anchor, 'soda_anchor_img_align', true );
				$soda_anchor_img_txt_type = get_post_meta( $anchor, 'soda_anchor_img_txt_type', true );
				$soda_anchor_img_txt_align = get_post_meta( $anchor, 'soda_anchor_img_txt_align', true );
				$soda_anchor_img_txt_nl = get_post_meta( $anchor, 'soda_anchor_img_txt_nl', true );
				$soda_anchor_img_txt_en = get_post_meta( $anchor, 'soda_anchor_img_txt_en', true );
				$soda_anchor_img_txt_nl_2 = get_post_meta( $anchor, 'soda_anchor_img_txt_nl_2', true );
				$soda_anchor_img_txt_en_2 = get_post_meta( $anchor, 'soda_anchor_img_txt_en_2', true );
				$soda_anchor_video = get_post_meta( $anchor, 'soda_anchor_video', true );
				$soda_anchor_video_embed = get_post_meta( $anchor, 'soda_anchor_video_embed', true );
				$soda_anchor_video_img = get_post_meta( $anchor, 'soda_anchor_video_img', true );
				$soda_anchor_video_txt_nl = get_post_meta( $anchor, 'soda_anchor_video_txt_nl', true );
				$soda_anchor_video_txt_en = get_post_meta( $anchor, 'soda_anchor_video_txt_en', true );
				$soda_anchor_order = get_post_meta( $anchor, 'soda_anchor_order', true );
				
				$soda_linker_kolom_id = get_post_meta( $anchor, 'soda_linker_kolom_id', true );
								
				if($count>0){
					if(!is_array($soda_anchor_order)||count($soda_anchor_order)!=$count){
						$soda_anchor_order = array(); // ++ added
						$order = array();
						for($x=$count-1;$x>=0;$x--){
							$soda_anchor_order[] = $x;
						}
					}
					
					$order = $soda_anchor_order;
					asort($order);

					foreach($order as $key=>$value) {
						$x = $key;
						
						echo '<div class="anchor anchor-'.$anchor.'" data-id="'.sanitize_title(get_the_title($anchor)).'">';
					
						if(!empty($soda_anchor_img[$x])&&$soda_anchor_img[$x]!='none'){ //=image
							if($soda_anchor_img_size[$x]=='small'){
								echo '<div class="img-container '.$soda_anchor_img_align[$x].' small">';
								echo wp_get_attachment_image($soda_anchor_img[$x], 'soda_small');
								echo wp_get_attachment_image($soda_anchor_img_2[$x], 'soda_small');
								echo '<div class="image-txt '.$soda_anchor_img_txt_type[$x].' small">';
								if($lng=='en'){
									if($soda_anchor_img_txt_en[$x]!='none'){
										echo '<div class="small small-img-text-left english" lang="en" id="soda_anchor_img_txt_en_'.$soda_anchor_img[$x].'">'.$soda_anchor_img_txt_en[$x].'</div>';
									}
									if($soda_anchor_img_txt_en_2[$x]!='none'){
										echo '<div class="small small-img-text-right english" lang="en" id="soda_anchor_img_txt_en_2_'.$soda_anchor_img[$x].'">'.$soda_anchor_img_txt_en_2[$x].'</div>';
									}
								}else{
									if($soda_anchor_img_txt_nl[$x]!='none'){
										echo '<div class="small small-img-left dutch" lang="nl" id="soda_anchor_img_txt_nl_'.$soda_anchor_img[$x].'">'.$soda_anchor_img_txt_nl[$x].'</div>';
									}
									if($soda_anchor_img_txt_nl_2[$x]!='none'){
										echo '<div class="small small-img-text-right dutch" lang="nl" id="soda_anchor_img_txt_nl_2_'.$soda_anchor_img[$x].'">'.$soda_anchor_img_txt_nl_2[$x].'</div>';
									}
								}
								if($soda_anchor_img_size[$x]=='small'){
									echo '<br class="clearfix"></div></div>';
								}else{
									echo '<br class="clearfix"></div>';
								}
							}else{
								echo '<div class="img-container '.$soda_anchor_img_align[$x].' '.$soda_anchor_img_size[$x].'">';
								echo wp_get_attachment_image($soda_anchor_img[$x], 'soda_'.$soda_anchor_img_size[$x]);
								/*$img_1 =  wp_get_attachment_image_src($soda_anchor_img[$x], $soda_anchor_img_size[$x]);
								echo '<div style="background-image:url(\''.$img_1[0].'\');"></div>';*/
								echo '<div class="image-txt '.$soda_anchor_img_txt_type[$x].' '.$soda_anchor_img_txt_align[$x].' '.$soda_anchor_img_size[$x].'">';
								if($lng=='en'){
									if($soda_anchor_img_txt_en[$x]!='none'){
										echo '<div class="english" lang="en" id="soda_anchor_img_txt_en_'.$soda_anchor_img[$x].'">';
										if($soda_anchor_img_txt_type[$x]=='slide'){
											$txt = explode("[*|slidingpoint|*]", $soda_anchor_img_txt_en[$x]);
											echo $txt[0].'</div><a class="more-text">&gt;</a><div class="slide-text hide"><div>'.$txt[1].'<a class="less-text">&lt;</a></div>';
										}else{
											echo $soda_anchor_img_txt_en[$x];
										}
										echo '</div>';
									}
								}else{
									if($soda_anchor_img_txt_nl[$x]!='none'){
										echo '<div class="dutch" lang="nl" id="soda_anchor_img_txt_nl_'.$soda_anchor_img[$x].'">';
										if($soda_anchor_img_txt_type[$x]=='slide'){
											$txt = explode("[*|slidingpoint|*]", $soda_anchor_img_txt_nl[$x]);
											echo $txt[0].'</div><a class="more-text">&gt;</a><div class="slide-text hide"><div>'.$txt[1].'<a class="less-text">&lt;</a></div>';
										}else{
											echo $soda_anchor_img_txt_nl[$x];
										}
										echo '</div>';
									}
								}
								echo '</div></div>';
							}
							
						}else{ //=video
							if($soda_anchor_video[$x]=='embedded'){ //=embed
								echo '<div class="video-embed">'.$soda_anchor_video_embed[$x].'</div>';
							}else{ //=linked
								echo wp_get_attachment_image($soda_anchor_video_img[$x], 'soda_large');								
							}
							echo '<div class="image-txt">';
							if($lng=='en'){
								if($soda_anchor_video_txt_en[$x]!='none'){
									echo '<div id="soda_anchor_video_txt_en_'.$soda_anchor_video_img[$x].'" lang="en" class="english">'.$soda_anchor_video_txt_en[$x].'</div>';
								}
							}else{
								if($soda_anchor_video_txt_nl[$x]!='none'){
									echo '<div id="soda_anchor_video_txt_nl_'.$soda_anchor_video_img[$x].'" lang="nl" class="dutch">'.$soda_anchor_video_txt_nl[$x].'</div>';
								}
							}
							echo '</div>';
						}
					
						echo '</div>';
						
					}
				}
				// left column
				if(!empty($soda_linker_kolom_id)){
					
					$soda_left_column_type = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_type', true );
					$soda_left_column_img = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img', true );
					$soda_left_column_img_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img_2', true );
					$soda_left_column_txt_nl = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_nl', true );
					$soda_left_column_txt_en = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_en', true );
					$soda_left_column_txt_nl_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_nl_2', true );
					$soda_left_column_txt_en_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_en_2', true );
					$soda_left_column_img_big = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img_big', true );

					if($soda_left_column_type!='none'){

						echo '<div class="left-column '.$soda_left_column_type.'" data-anchor="anchor-'.$anchor.'">'; //transparent

						if($soda_left_column_type=='quote'){
							if($lng=='en'){
								echo $soda_left_column_txt_en;
							}else{
								echo $soda_left_column_txt_nl;
							}
						}
						if($soda_left_column_type=='image'){
							echo wp_get_attachment_image($soda_left_column_img, 'soda_medium');
							if($lng=='en'){
								echo '<div class="caption">'.$soda_left_column_txt_en.'</div>';
							}else{
								echo '<div class="caption">'.$soda_left_column_txt_nl.'</div>';
							}
						}
						if($soda_left_column_type=='small-image'){
							echo '<div class="img-container small">';
							echo wp_get_attachment_image($soda_left_column_img, 'soda_small');
							echo wp_get_attachment_image($soda_left_column_img_2, 'soda_small');
							echo '<div class="image-txt">';
							if($lng=='en'){
								if($soda_left_column_txt_en!='none'){
									echo '<div class="small small-img-text-left english">'.$soda_left_column_txt_en.'</div>';
								}
								if($soda_left_column_txt_en_2!='none'){
									echo '<div class="small small-img-text-right english">'.$soda_left_column_txt_en_2.'</div>';
								}
							}else{
								if($soda_left_column_txt_nl!='none'){
									echo '<div class="small small-img-left dutch">'.$soda_left_column_txt_nl.'</div>';
								}
								if($soda_left_column_txt_nl_2!='none'){
									echo '<div class="small small-img-text-right dutch">'.$soda_left_column_txt_nl_2.'</div>';
								}
							}
							echo '<br class="clearfix"></div></div>';
						}
						if($soda_left_column_type=='image-left-column'){
							echo '<div class="image-left-side">'.wp_get_attachment_image($soda_left_column_img_big, 'soda_leftcolumn').'</div>';
						}

						echo '</div>';
					}

				}
			}
		}
	?>
	
	<div class="center-line blauwgrijs"></div>

</section>
<!-- /section -->

<?php get_footer(); ?>