<?php
/*
 *  Author: Joris Lindhout
 *  URL: soda.nl
 *  Custom functions, support, custom post types and more.
 */

/*
 * ========================================================================
 * Theme Support
 * ========================================================================
 */

if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
}

/*
 * ========================================================================
 * Functions
 * ========================================================================
 */

function soda_scripts() {
    if (!is_admin()) {
        wp_deregister_script('jquery'); // Deregister WordPress jQuery
        wp_register_script('jquery', 'https://code.jquery.com/jquery-1.11.2.min.js', array(), '1.11.2');
        wp_enqueue_script('jquery');

        wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), '2.8.3');
        wp_enqueue_script('modernizr'); 
        
        wp_register_script('conditionizr', 'https://cdnjs.cloudflare.com/ajax/libs/conditionizr.js/4.1.0/conditionizr.min.js', array('jquery'), '4.1.0');
        wp_enqueue_script('conditionizr');

		wp_register_script('soda_scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', false); // Custom scripts 
        wp_enqueue_script('soda_scripts');
    }
}

// Theme Stylesheets using Enqueue
function soda_styles(){
    wp_register_style('normalize', get_template_directory_uri() . '/css/normalize.min.css', 'normalize', '3.0.2', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

	//wp_enqueue_style( 'dashicons', get_stylesheet_uri(), array( 'dashicons' ), '1.0' );
    
    wp_register_style('soda', get_template_directory_uri() . '/style.css', 'style', '1.0', 'all');
    wp_enqueue_style('soda'); // Enqueue it!	
}

// Load Optimised Google Analytics in the footer
function soda_add_google_analytics() {
    $google = "<!-- Optimised Asynchronous Google Analytics -->";
    $google .= "<script>";
    $google .= "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'XXX', 'auto');
	  ga('send', 'pageview');";
    $google .= "</script>";
    echo $google;
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function soda_my_css_attributes_filter($var) {
    return is_array($var) ? array() : '';
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function soda_add_slug_to_body_class($classes) {
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
		$classes[] = sanitize_html_class(get_query_var('args'));
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
		$classes[] = sanitize_html_class(get_query_var('args'));
    }

    return $classes;
}

// Remove 'text/css' from our enqueued stylesheet
function soda_style_remove($tag) {
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

function soda_remove_header_info() {
	//remove_action('wp_head', 'feed_links', 2);  //removes feeds
	remove_action('wp_head', 'feed_links_extra', 3);  //removes comment feed links
}

function soda_terms_clauses($clauses, $taxonomy, $args) {
	if (!empty($args['post_type']))	{
		global $wpdb;

		$post_types = array();

		foreach($args['post_type'] as $cpt)	{
			$post_types[] = "'".$cpt."'";
		}

	    if(!empty($post_types))	{
			$clauses['fields'] = 'DISTINCT '.str_replace('tt.*', 'tt.term_taxonomy_id, tt.term_id, tt.taxonomy, tt.description, tt.parent', $clauses['fields']).', COUNT(t.term_id) AS count';
			$clauses['join'] .= ' INNER JOIN '.$wpdb->term_relationships.' AS r ON r.term_taxonomy_id = tt.term_taxonomy_id INNER JOIN '.$wpdb->posts.' AS p ON p.ID = r.object_id';
			$clauses['where'] .= ' AND p.post_type IN ('.implode(',', $post_types).')';
			$clauses['orderby'] = 'GROUP BY t.term_id '.$clauses['orderby'];
		}
    }
    return $clauses;
}

function soda_add_parent_url_menu_class( $classes = array(), $item = false ) {
	$current_url = soda_current_url();
	$homepage_url = trailingslashit( get_bloginfo( 'url' ) );
	if( is_404() or $item->url == $homepage_url ) return $classes;
	// if ($item->url=='http://soda.nl/soda-plus/' && strstr( $current_url, 'http://soda.nl/plus/')){
	// 	$classes[] = 'active';
	// }
	// if ($item->url=='http://soda.nl/bureau/' && strstr( $current_url, 'http://soda.nl/bureau/')){
	// 	$classes[] = 'active';
	// }
	if ( strstr( $current_url, $item->url) ) {
		$classes[] = 'active';
	}
	return $classes;
}

function soda_current_url() {
	if(in_array('HTTPS', $_SERVER)){
		$url = ( 'on' == $_SERVER['HTTPS'] ) ? 'https://' : 'http://';
	}else{
		$url = 'http://';
	}
	$url .= $_SERVER['SERVER_NAME'];
	$url .= ( '80' == $_SERVER['SERVER_PORT'] ) ? '' : ':' . $_SERVER['SERVER_PORT'];
	$url .= $_SERVER['REQUEST_URI'];
	return trailingslashit( $url );
}

/*
 * ========================================================================
 * Admin
 * ========================================================================
 */

function soda_thumb_size() {
	add_image_size('soda_small', 230); 
	add_image_size('soda_medium', 470); 
	add_image_size('soda_large', 635);
	add_image_size('soda_leftcolumn', 150, 9999, true);  
}

function soda_register_menus() {
	register_nav_menu('menu',__( 'Menu' ));
}

function makeTextareaButtons(){
	echo '<input type="button" value="Link" onclick="jQuery(\'.textarea.active\').focus(); pasteHtmlAtCaret(prompt(\'Link parameters:\', \'<a href=&quot;URL&quot; target=&quot;_blank&quot;>LINK TEKST</a>\')); "> <input type="button" value="Titel" onclick="jQuery(\'.textarea.active\').focus(); pasteHtmlAtCaret(prompt(\'Titel:\', \'<h3>TITEL</h3>\')); "> <input type="button" value="Tussenkopje" onclick="jQuery(\'.textarea.active\').focus(); pasteHtmlAtCaret(prompt(\'Tussenkopje:\', \'<h4>TUSSENKOP</h4>\')); "> <input type="button" value="Bijschrift" onclick="jQuery(\'.textarea.active\').focus(); pasteHtmlAtCaret(prompt(\'bijschrift:\', \'<h5>BIJSCHRIFT</h5>\')); "> <input type="button" value="Slider breekpunt" onclick="jQuery(\'.textarea.active\').focus(); pasteHtmlAtCaret(\'[*|slidingpoint|*]\'); " class="hide slider-button"> <input type="button" value="Verwijder alle content" onclick="jQuery(\'.textarea.active\').focus(); clearContenteditable(); " class="clear-contenteditable">';
}

function soda_homepage_post_type($postType) {
	add_meta_box(
			'soda_homepage_post_type',
			'Type of post',
			'soda_homepage',
			'post',
			'normal',
			'high'
	);
}
function soda_homepage($post) {
	wp_nonce_field( plugin_basename( __FILE__ ), 'soda_homepage_nonce' );
	
	$soda_post_type = get_post_meta( get_the_ID(), 'soda_post_type', true );
	$soda_anchor_img = get_post_meta( get_the_ID(), 'soda_anchor_img', true );
	$soda_anchor_img_2 = get_post_meta( get_the_ID(), 'soda_anchor_img_2', true );
	$soda_anchor_img_size = get_post_meta( get_the_ID(), 'soda_anchor_img_size', true );
	$soda_anchor_img_align = get_post_meta( get_the_ID(), 'soda_anchor_img_align', true );
	$soda_anchor_img_txt_type = get_post_meta( get_the_ID(), 'soda_anchor_img_txt_type', true );
	$soda_anchor_img_txt_align = get_post_meta( get_the_ID(), 'soda_anchor_img_txt_align', true );
	$soda_anchor_img_txt = get_post_meta( get_the_ID(), 'soda_anchor_img_txt', true );
	$soda_anchor_img_txt_2 = get_post_meta( get_the_ID(), 'soda_anchor_img_txt_2', true );
	$soda_anchor_video = get_post_meta( get_the_ID(), 'soda_anchor_video', true );
	$soda_anchor_video_embed = get_post_meta( get_the_ID(), 'soda_anchor_video_embed', true );
	$soda_anchor_video_img = get_post_meta( get_the_ID(), 'soda_anchor_video_img', true );
	$soda_anchor_video_txt = get_post_meta( get_the_ID(), 'soda_anchor_video_txt', true );
	?>
	<div class="anchor selected homepage">
		<fieldset>
			<select class="soda_post_type" name="soda_post_type">
				<?php echo '<option value="'.$soda_post_type.'" selected>'.ucfirst($soda_post_type).'</option>'; ?>
				<option value="image">Image</option>
				<option value="video">Video</option>
			</select>
		</fieldset>
		<?php
			$img_class = 'hide';
			$vid_class = 'hide';
			if($soda_post_type=='image'){
				$img_class = '';
			}else if($soda_post_type=='video'){
				$vid_class = '';
			}
		?>
		<div class="anchor-item selected">
			<div class="anchor-image <?php echo $img_class; ?>">
				<?php 
					$img_small_class = '';
					if($soda_anchor_img_size!='small'){
						$img_small_class = 'hide';
					}
				?>
				<fieldset>
					<legend>Image</legend>
					<input type="hidden" class="soda_anchor_img" name="soda_anchor_img" value="<?php echo $soda_anchor_img; ?>">
					<input type="hidden" class="soda_anchor_img_2" name="soda_anchor_img_2" value="<?php echo $soda_anchor_img_2; ?>">
					<div class="thumb-container"> 
						<?php
							echo wp_get_attachment_image( $soda_anchor_img, 'small' );
							echo wp_get_attachment_image( $soda_anchor_img_2, 'small' );
						?>
					</div>
					<a class="remove_image">Remove image(s)</a>
					<label>Image size</label>
					<select class="image-size soda_anchor_img_size" name="soda_anchor_img_size">
						<?php echo '<option value="'.$soda_anchor_img_size.'" selected>'.ucfirst($soda_anchor_img_size).'</option>'; ?>
						<option value="large">Large</option>
						<option value="medium">Medium</option>
						<option value="small">Small</option>
					</select><br>
					<div class="upload-image-buttons">
						<input type="button" value="Upload image 1" data-type="image" data-dest="soda_anchor_img">
						<input type="button" value="Upload image 2" data-type="image" data-dest="soda_anchor_img_2" class="<?php echo $img_small_class; ?> upload-image-2-button">
					</div>
					<label>Image align</label>
					<select class="image-align soda_anchor_img_align" name="soda_anchor_img_align">
						<?php echo '<option value="'.$soda_anchor_img_align.'" selected>'.ucfirst($soda_anchor_img_align).'</option>'; ?>
						<option value="left">Left</option>
						<option value="behind">Behind</option>
						<option value="right">Right</option>
					</select><br>
					<label>Image text type</label>
					<select class="text-type soda_anchor_img_txt_type" name="soda_anchor_img_txt_type">
						<?php echo '<option value="'.$soda_anchor_img_txt_type.'" selected>'.ucfirst($soda_anchor_img_txt_type).'</option>'; ?>
						<option value="static">Static</option>
						<option value="slide">Slide</option>
						<option value="caption">Caption</option>
						<option value="small">Small</option>
					</select><br>
					<label>Image text align</label>
					<select class="text-align soda_anchor_img_txt_align" name="soda_anchor_img_txt_align">
						<?php echo '<option value="'.$soda_anchor_img_txt_align.'" selected>'.ucfirst($soda_anchor_img_txt_align).'</option>'; ?>
						<option value="behind">Behind</option>
						<option value="right">Right</option>
					</select><br>
				</fieldset>
				<fieldset>
					<legend>Image text</legend>
					<?php
						wp_editor( $soda_anchor_img_txt, 'soda_anchor_img_txt', array(
					        'wpautop'       => true,
					        'media_buttons' => false,
					        'textarea_name' => 'soda_anchor_img_txt',
					        'textarea_rows' => 10,
					        'teeny'         => true,
							'dfw'			=> true,
							'class'  => 'wp-editor-area'
					    ) );
					?>
					<div class="second-txt-field <?php echo $img_small_class; ?>">
						<?php
							wp_editor( $soda_anchor_img_txt_2, 'soda_anchor_img_txt_2', array(
						        'wpautop'       => true,
						        'media_buttons' => false,
						        'textarea_name' => 'soda_anchor_img_txt_2',
						        'textarea_rows' => 10,
						        'teeny'         => true,
								'dfw'			=> true,
								'class'  => 'wp-editor-area'
						    ) );
						?>
					</div>
				</fieldset>
			</div>
			<div class="anchor-video <?php echo $vid_class; ?>">
				<fieldset>
					<legend>Video</legend>
					<select class="video-type soda_anchor_video" name="soda_anchor_video">
						<?php 
							$linked_class = '';
							$embed_class = '';
							if($soda_anchor_video=='linked'){
								$embed_class = 'hide';
							}
							if($soda_anchor_video=='embedded'){
								$linked_class = 'hide';
							}
							if($soda_anchor_video=='none'){
								$linked_class = 'hide';
								$embed_class = 'hide';
							}
							echo '<option value="'.$soda_anchor_video.'" selected>'.ucfirst($soda_anchor_video).'</option>'; 
						?>
						<option value="none">No video</option>
						<option value="linked">Linked</option>
						<option value="embedded">Embedded</option>
					</select>
					<div class="video-embed <?php echo $embed_class; ?>">
						<label>Embed code</label><br><textarea class="soda_anchor_video_embed" name="soda_anchor_video_embed"><?php echo $soda_anchor_video_embed; ?></textarea>
					</div>
				</fieldset>
				<fieldset class="video-image <?php echo $linked_class; ?>">
					<legend>Video image</legend>
					<input type="hidden" class="soda_anchor_video_img" name="soda_anchor_video_img" value="<?php echo $soda_anchor_video_img; ?>">
					<div class="thumb-container">
						<?php
							echo wp_get_attachment_image( $soda_anchor_video_img, 'small' );
						?>
					</div>
					<input type="button" value="Upload video image" data-type="video" data-dest="soda_anchor_video_img">
				</fieldset>
				<fieldset>
					<legend>Video text</legend>
					<?php
						wp_editor( $soda_anchor_video_txt, 'soda_anchor_video_txt', array(
					        'wpautop'       => true,
					        'media_buttons' => false,
					        'textarea_name' => 'soda_anchor_video_txt',
					        'textarea_rows' => 10,
					        'teeny'         => true,
							'dfw'			=> true,
							'class'  => 'wp-editor-area'
					    ) );
					?>
				</fieldset>
			</div>
		</div>	
	</div>	
	<?php
	
}
function soda_homepage_save( $post_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
		return;
	
		if ( !isset($_POST['soda_homepage_nonce']) || !wp_verify_nonce( $_POST['soda_homepage_nonce'], plugin_basename( __FILE__ ) ) )
		return;
	
		if ( !current_user_can( 'edit_page', $post_id ) )
		return;

		$soda_post_type = $_POST['soda_post_type'];
		update_post_meta($post_id, 'soda_post_type', $soda_post_type);
		
		$soda_anchor_img = $_POST['soda_anchor_img'];
		update_post_meta($post_id, 'soda_anchor_img', $soda_anchor_img);
		
		$soda_anchor_img_2 = $_POST['soda_anchor_img_2'];
		update_post_meta($post_id, 'soda_anchor_img_2', $soda_anchor_img_2);
		
		$soda_anchor_img_size = $_POST['soda_anchor_img_size'];
		update_post_meta($post_id, 'soda_anchor_img_size', $soda_anchor_img_size);
		
		$soda_anchor_img_align = $_POST['soda_anchor_img_align'];
		update_post_meta($post_id, 'soda_anchor_img_align', $soda_anchor_img_align);
		
		$soda_anchor_img_txt_type = $_POST['soda_anchor_img_txt_type'];
		update_post_meta($post_id, 'soda_anchor_img_txt_type', $soda_anchor_img_txt_type);
		
		$soda_anchor_img_txt_align = $_POST['soda_anchor_img_txt_align'];
		update_post_meta($post_id, 'soda_anchor_img_txt_align', $soda_anchor_img_txt_align);
		
		$soda_anchor_img_txt = $_POST['soda_anchor_img_txt'];
		update_post_meta($post_id, 'soda_anchor_img_txt', $soda_anchor_img_txt);
		
		$soda_anchor_img_txt_2 = $_POST['soda_anchor_img_txt_2'];
		update_post_meta($post_id, 'soda_anchor_img_txt_2', $soda_anchor_img_txt_2);
		
		$soda_anchor_video = $_POST['soda_anchor_video'];
		update_post_meta($post_id, 'soda_anchor_video', $soda_anchor_video);
		
		$soda_anchor_video_embed = $_POST['soda_anchor_video_embed'];
		update_post_meta($post_id, 'soda_anchor_video_embed', $soda_anchor_video_embed);
		
		$soda_anchor_video_img = $_POST['soda_anchor_video_img'];
		update_post_meta($post_id, 'soda_anchor_video_img', $soda_anchor_video_img);
		
		$soda_anchor_video_txt = $_POST['soda_anchor_video_txt'];
		update_post_meta($post_id, 'soda_anchor_video_txt', $soda_anchor_video_txt);
}


function soda_left_column_page($postType) {
	add_meta_box(
			'soda_left_column_page',
			'Linker kolom',
			'soda_left_column',
			'page',
			'normal',
			'high'
	);
}
function soda_left_column($post) {
	wp_nonce_field( plugin_basename( __FILE__ ), 'soda_left_column_nonce' );
	$soda_linker_kolom_id = get_post_meta( get_the_ID(), 'soda_linker_kolom_id', true );
	?>
	
	<div class="admin-linker-kolom-container selected anchor">
		<input type="hidden" value="<?php echo $soda_linker_kolom_id; ?>" name="soda_linker_kolom_id">
		<input type="hidden" value="<?php echo get_the_ID(); ?>" class="soda_anchor_id">
		<input type="hidden" value="<?php echo get_the_title(); ?>" class="soda_anchor_title_nl">
		<?php
			if(!empty($soda_linker_kolom_id)){
				$soda_left_column_type = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_type', true );
				$soda_left_column_img = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img', true );
				$soda_left_column_img_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img_2', true );
				$soda_left_column_txt_nl = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_nl', true );
				$soda_left_column_txt_en = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_en', true );
				$soda_left_column_txt_nl_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_nl_2', true );
				$soda_left_column_txt_en_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_en_2', true );
				$soda_left_column_img_big = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img_big', true );
			}else{
				$soda_left_column_type = '';
				$soda_left_column_img = '';
				$soda_left_column_img_2 = '';
				$soda_left_column_txt_nl = '';
				$soda_left_column_txt_en = '';
				$soda_left_column_txt_nl_2 = '';
				$soda_left_column_txt_en_2 = '';
				$soda_left_column_img_big = '';
			}
			?>
			<div id="admin-linker-kolom-<?php echo $soda_linker_kolom_id; ?>" class="admin-linker-kolom">
			<h3 class="linker-kolom-header"><a class="minify"><span class="dashicons dashicons-menu"></span></a>Linker kolom<a class="delete reset-linker-kolom"><span class="dashicons dashicons-no-alt"></span></a></h3>
			<input type="hidden" class="soda_linker_kolom_anker_id" value="<?php echo $soda_linker_kolom_id; ?>">
			<?php
				$quote_img_class = '';
				$img_class = '';
				$small_img_class = '';
				$big_img_class = '';
				if($soda_left_column_type=='quote'){
					$quote_img_class = 'hide';
				}
				if($soda_left_column_type!='image'&&$soda_left_column_type!='small-image'&&$soda_left_column_type!='small-image-quote'&&$soda_left_column_type!='image-left-column'){
					$img_class = 'hide';
				}
				if($soda_left_column_type=='image'){
					$img_norm_class = 'hide';
				}
				if($soda_left_column_type=='small-image'&&$soda_left_column_type=='small-image-quote'){
					$img_small_class = 'hide';
				}
				if($soda_left_column_type=='image-right-column'){
					$img_big_class = 'hide';
				}
				if($soda_left_column_type=='none'){
					$quote_img_class = 'hide';
					$img_class = 'hide';
					$img_small_class = 'hide';
					$img_norm_class = 'hide';
					$img_big_class = 'hide';
				}
				
			?>
			<div class="minifyable">
				<fieldset>
					<legend>Left column type</legend>
					<select class="left-column-type soda_left_column_type">
						<?php echo '<option value="'.$soda_left_column_type.'" selected>'.ucfirst($soda_left_column_type).'</option>';  ?>
						<option value="none">No left column</option>
						<option value="quote">Quote</option>
						<option value="image">Image</option>
						<option value="small-image">Small image</option>
						<!--<option value="small-image-quote">Small image &amp; Quote</option>-->
						<option value="image-left-column">Image in left column</option>
					</select>
				</fieldset>
				<fieldset class="left-column-images <?php echo $img_class; ?>">
					<legend>Left column image</legend>
					<input type="hidden" class="soda_left_column_img" value="<?php echo $soda_left_column_img; ?>">
					<input type="hidden" class="soda_left_column_img_2" value="<?php echo $soda_left_column_img_2; ?>">
					<input type="hidden" class="soda_left_column_img_big" value="<?php echo $soda_left_column_img_big; ?>">
					<div class="thumb-container"> 
						<?php
							echo wp_get_attachment_image( $soda_left_column_img, 'small' );
							echo wp_get_attachment_image( $soda_left_column_img_2, 'small' );
							echo wp_get_attachment_image( $soda_left_column_img_big, 'small' );
						?>
					</div>
					<a class="remove_image_linker_kolom">Remove image(s)</a>
					<div class="upload-image-buttons">
						<input type="button" value="Upload image 1" data-type="left-column" data-dest="soda_left_column_img" class="left_column_img <?php echo $img_big_class; ?>">
						<input type="button" value="Upload image 2" data-type="left-column" data-dest="soda_left_column_img_2" class="left_column_img_2 <?php echo $img_norm_class; ?><?php echo $img_big_class; ?>">
						<input type="button" value="Upload big image" data-type="left-column" data-dest="soda_left_column_img_big" class="left_column_img_big <?php echo $img_norm_class; ?><?php echo $img_small_class; ?>">
					</div>
				</fieldset>
				<fieldset class="left-column-text <?php echo $img_big_class; ?>">
					<legend>Left column text</legend>
					<?php makeTextareaButtons(); ?>
					<div class="soda_left_column_txt_nl soda-admin-translatable-field dutch textarea" lang="nl" contenteditable="true"><?php echo $soda_left_column_txt_nl; ?></div>
					<div class="soda_left_column_txt_en soda-admin-translatable-field english textarea" lang="en" contenteditable="true"><?php echo $soda_left_column_txt_en; ?></div>
					<div class="second-txt-field <?php echo $small_img_class; ?>">
						<?php makeTextareaButtons(); ?>
						<div class="soda_left_column_txt_nl_2 soda-admin-translatable-field dutch textarea" lang="nl" contenteditable="true"><?php echo $soda_left_column_txt_nl_2; ?></div>
						<div class="soda_left_column_txt_en_2 soda-admin-translatable-field english textarea" lang="en" contenteditable="true"><?php echo $soda_left_column_txt_en_2; ?></div>
					</div>
				</fieldset>
				<input type="button" value="Update linker kolom" class="save-left-column">
			</div> <!-- /linkerkolom minifyable -->
		</div> <!-- /linkerkolom -->
	</div> <!-- /linkerkolom container -->
	
	<?php
	
}
function soda_left_column_save( $post_id ) {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
		return;
	
		if ( !isset($_POST['soda_left_column_nonce']) || !wp_verify_nonce( $_POST['soda_left_column_nonce'], plugin_basename( __FILE__ ) ) )
		return;
	
		if ( !current_user_can( 'edit_page', $post_id ) )
		return;

		$soda_linker_kolom_id = $_POST['soda_linker_kolom_id'];
		update_post_meta($post_id, 'soda_linker_kolom_id', $soda_linker_kolom_id);
}

function linker_kolom_homepage(){
	include 'left-column-homepage.php';
}
function soda_add_homepage_submenu(){
	add_submenu_page( 'edit.php', 'Linker kolom homepage', 'Linker kolom homepage', 'edit_posts', 'linker-kolom-homepage', 'linker_kolom_homepage');
}

function soda_admin_js_css() {
	wp_enqueue_script('soda-js', get_bloginfo('template_url') . '/js/soda-admin.js');
	wp_enqueue_style('soda-css', get_bloginfo('template_directory') . '/css/soda-admin.css');
	if(isset($_GET['page'])&&$_GET['page']=='linker-kolom-homepage'){
		wp_enqueue_media();
		wp_enqueue_script( 'custom-header' );
	}
}

function soda_save_revision_metadata($post_id, $post) {
	$parent_id = wp_is_post_revision($post_id);
	if($parent_id) {
		$parent  = get_post($parent_id);
		$soda_anchor_id = get_post_meta( $parent->ID, 'soda_anchor_id', true );
		$soda_linker_kolom_anker_id = get_post_meta( $parent->ID, 'soda_linker_kolom_anker_id', true );
		if(false !== $soda_anchor_id){
			add_metadata( 'post', $post_id, 'soda_anchor_id', $soda_anchor_id );
			add_metadata( 'post', $post_id, 'soda_linker_kolom_anker_id', $soda_linker_kolom_anker_id );
		}
	}
}
function soda_restore_revision_metadata($post_id, $revision_id){
	$post = get_post( $post_id );
	$revision = get_post( $revision_id );
	$soda_anchor_id = get_metadata( 'post', $revision->ID, 'soda_anchor_id', true );
	$soda_linker_kolom_anker_id = get_metadata( 'post', $revision->ID, 'soda_linker_kolom_anker_id', true );

	if ( false !== $soda_anchor_id )
		update_post_meta( $post_id, 'soda_anchor_id', $soda_anchor_id );
	else
		delete_post_meta( $post_id, 'soda_anchor_id' );
		
	if ( false !== $soda_linker_kolom_anker_id )
		update_post_meta( $post_id, 'soda_linker_kolom_anker_id', $soda_linker_kolom_anker_id );
	else
		delete_post_meta( $post_id, 'soda_linker_kolom_anker_id' );
}
function soda_revision_fields_metadata( $fields ) {
	$fields['soda_anchor_id'] = 'Anker ID';
	$fields['soda_linker_kolom_anker_id'] = 'Linker kolom ID';
	return $fields;
}
function soda_revision_field_metadata( $value, $field ) {
	global $revision;
	$c = get_metadata( 'post', $revision->ID, $field, true );
	$return = '';
	foreach((array) $c as $item){
		$return = $return.' '.$item.',';
	}
	return $return;
}
function soda_post_remove(){ 
	remove_menu_page('edit-comments.php');
}

function soda_disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}
function soda_disable_wp_emojicons() {
  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'soda_disable_emojicons_tinymce' );
}


/*
 * ========================================================================
 * Actions + Filters + ShortCodes
 * ========================================================================
 */

// Add Actions
add_action('wp_enqueue_scripts', 'soda_scripts'); // Add Custom Scripts
add_action('wp_footer', 'soda_add_google_analytics'); // Google Analytics optimised in footer
add_action('wp_enqueue_scripts', 'soda_styles'); // Add Theme Stylesheet
add_action('init', 'soda_remove_header_info');
add_action('init', 'soda_disable_wp_emojicons');
add_action('init', 'soda_register_menus');
add_action('after_setup_theme','soda_thumb_size');
add_action('admin_enqueue_scripts', 'soda_admin_js_css');
add_action('admin_menu', 'soda_post_remove');
add_action('admin_menu', 'soda_add_homepage_submenu');
add_action('add_meta_boxes', 'soda_homepage_post_type');
add_action('save_post', 'soda_homepage_save');
add_action('add_meta_boxes', 'soda_left_column_page');
add_action('save_post', 'soda_left_column_save');
add_action('save_post','soda_save_revision_metadata', 10, 2 );
add_action( 'wp_restore_post_revision', 'soda_restore_revision_metadata', 10, 2 );

// Remove Actions
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('body_class', 'soda_add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('style_loader_tag', 'soda_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('terms_clauses', 'soda_terms_clauses', 10, 3);
add_filter('nav_menu_css_class', 'soda_add_parent_url_menu_class', 10, 2);
add_filter( '_wp_post_revision_fields', 'soda_revision_fields_metadata' );
add_filter( '_wp_post_revision_field_soda_anchor_id', 'soda_revision_field_metadata', 10, 2 );
add_filter( '_wp_post_revision_field_soda_linker_kolom_anker_id', 'soda_revision_field_metadata', 10, 2 );

?>
