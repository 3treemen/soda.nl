(function($) {
  var cache = [];
  // Arguments are image paths relative to the current page.
  $.preLoadImages = function() {
    var args_len = arguments.length;
    for (var i = args_len; i--;) {
      var cacheImage = document.createElement('img');
      cacheImage.src = arguments[i];
      cache.push(cacheImage);
    }
  }
})(jQuery)

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

jQuery(document).ready(function($){
	
	$('.mobile-nav').click(function(e) {
		e.preventDefault();
		$(this).toggleClass('active');
		$('ul.menu').slideToggle();
	});
	
	$('.submenu.level-3.anchors a, .plus .level-2.anchors a, .bureau .level-2.anchors a').on('click',function(){
		$('.submenu.level-3.anchors li, .plus .level-2.anchors li, .bureau .level-2.anchors li').removeClass('active');
		$(this).parent().addClass('active');
	})
	
	$('.more-text').on('click',function(){
		$(this).next('.slide-text').slideDown();
		$(this).hide();
	})
	$('.less-text').on('click',function(){
		$(this).parent().slideUp();
		$(this).parent().parent().parent().find('.more-text').show();
	})
	
	$('.animate-scroll').on('click',function(e){
		e.preventDefault();
		id = $(this).attr('href').slice(1);
		$('html,body').animate({scrollTop:$('body').find("[data-id='" + id + "']").position().top}, 1000,'swing');
	})
	
	/*$('.submenu.level-1 li > a').not('.submenu.level-1.plus li > a, .submenu.level-1.bureau li > a').on('click',function(e){
		//console.log($(this).parent());
		//e.preventDefault();
		$('.submenu.level-2').not('.level-1 .active .submenu.level-2').slideUp();
		$(this).parent().find('.submenu.level-2').slideToggle();
		//console.log($(this).parent());
	})*/
	
	/*$('.menu-item > a').on('click',function(e){
		//console.log($(this).parent());
		//e.preventDefault();
		$('.submenu.level-1').slideUp();
		//$(this).parent().addClass('active');
		$(this).parent().find('.submenu.level-1').slideToggle();
	})*/


	$(window).scroll(function(){
		$(".left-column").not('.left-column-page').each(function(){
			var eTop = $('.'+$(this).attr('data-anchor')).first().offset().top;
			var total_height = 0;
			$('.'+$(this).attr('data-anchor')).each(function(){
				//console.log($(this).outerHeight());
				total_height += $(this).outerHeight();
			})
			var bottom_of_element = eTop + total_height;
			//console.log(eTop+ ' - '+total_height+' - '+bottom_of_element);
			//console.log($('.'+$(this).attr('data-anchor')).offset().top+' - '+$('.'+$(this).attr('data-anchor')).outerHeight());
			if(eTop<$(window).scrollTop()&&bottom_of_element>$(window).scrollTop()){
				$(this).css("opacity", 1 );
			}else{
				$(this).css("opacity", 0 );
				//$(this).css("opacity", 1 - (eTop - $(window).scrollTop()) / 500);
			}
		});
		
		$('.anchor').each(function(){
			var top_of_element = $(this).offset().top;
			var bottom_of_element = $(this).offset().top + $(this).outerHeight();
			var bottom_of_screen = $(window).scrollTop() + $(window).height();
			
			if((bottom_of_screen > top_of_element) && (bottom_of_screen < bottom_of_element)){
			    $('.submenu.level-3 li').removeClass('active');
				$('.submenu.level-3 li a[href="#'+$(this).attr('data-id')+'"]').parent().addClass('active');
			}
		})
		
	});
	
	$(document).on("click", ".loadmore.hide", function(e){
		return false;
	})
	$(document).on("click", ".loadmore:not(.loadmore.hide)", function(e) {
		if(e.preventDefault)e.preventDefault();
		else e.returnValue = false;
		var url = $(this).attr("href");
	    $.ajax({
				type: "POST",
				url: url,
				success: function(data){
					$('.loadmore').remove();
				    $('#anchor').append(data);
				},
				error: function(jqXHR, textStatus, errorThrown) {
					console.log('value','ERROR');
				}
		});
	})
	
});

