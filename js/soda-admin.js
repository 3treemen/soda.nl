var mediaUploader;
var dest;
var type;
var count;
var data_container;

function prepDataForSaving(){
	var count = jQuery('.admin-anchors-container .selected.anchor .anchor-item').length;
	//console.log(count);
	
	data_container = [];
	
	data_container.push(count);
	data_container.push(jQuery('.admin-anchors-container .selected.anchor .soda_anchor_title_nl').val());
	data_container.push(jQuery('.admin-anchors-container .selected.anchor .soda_anchor_title_en').val());
	
	var soda_anchor_img = [];
	var soda_anchor_img_2 = [];
	var soda_anchor_img_size = [];
	var soda_anchor_img_align = [];
	var soda_anchor_img_txt_type = [];
	var soda_anchor_img_txt_align = [];
	var soda_anchor_img_txt_nl = [];
	var soda_anchor_img_txt_en = [];
	var soda_anchor_img_txt_nl_2 = [];
	var soda_anchor_img_txt_en_2 = [];
	var soda_anchor_video = [];
	var soda_anchor_video_embed = [];
	var soda_anchor_video_img = [];
	var soda_anchor_video_txt_nl = [];
	var soda_anchor_video_txt_en = [];
	var soda_anchor_order = [];
	
	for(x=0;x<count;x++){
		
		soda_anchor_img[x] = 'none';
		soda_anchor_img_2[x] = 'none';
		soda_anchor_img_size[x] = 'none';
		soda_anchor_img_align[x] = 'none';
		soda_anchor_img_txt_type[x] = 'none';
		soda_anchor_img_txt_align[x] = 'none';
		soda_anchor_img_txt_nl[x] = 'none';
		soda_anchor_img_txt_en[x] = 'none';
		soda_anchor_img_txt_nl_2[x] = 'none';
		soda_anchor_img_txt_en_2[x] = 'none';
		soda_anchor_video[x] = 'none';
		soda_anchor_video_embed[x] = 'none';
		soda_anchor_video_img[x] = 'none';
		soda_anchor_video_txt_nl[x] = 'none';
		soda_anchor_video_txt_en[x] = 'none';
		soda_anchor_order[x] = '0';
		
		var anchor_item = jQuery('.admin-anchors-container .selected.anchor').find('.anchor-item[data-count='+x+']');
		if(anchor_item.find('.soda_anchor_img').length&&anchor_item.find('.soda_anchor_img').val()!=''){
			soda_anchor_img[x] = anchor_item.find('.soda_anchor_img').val();
		}			
		if(anchor_item.find('.soda_anchor_img_2').length&&anchor_item.find('.soda_anchor_img_2').val()!=''){
			soda_anchor_img_2[x] = anchor_item.find('.soda_anchor_img_2').val();
		}
		if(anchor_item.find('.soda_anchor_img_size').length&&anchor_item.find('.soda_anchor_img_size').val()!=''){
			soda_anchor_img_size[x] = anchor_item.find('.soda_anchor_img_size').val();
		}
		if(anchor_item.find('.soda_anchor_img_align').length&&anchor_item.find('.soda_anchor_img_align').val()!=''){
			soda_anchor_img_align[x] = anchor_item.find('.soda_anchor_img_align').val();
		}
		if(anchor_item.find('.soda_anchor_img_txt_type').length&&anchor_item.find('.soda_anchor_img_txt_type').val()!=''){
			soda_anchor_img_txt_type[x] = anchor_item.find('.soda_anchor_img_txt_type').val();
		}
		if(anchor_item.find('.soda_anchor_img_txt_align').length&&anchor_item.find('.soda_anchor_img_txt_align').val()!=''){
			soda_anchor_img_txt_align[x] = anchor_item.find('.soda_anchor_img_txt_align').val();
		}
		if(anchor_item.find('.soda_anchor_img_txt_nl').length&&anchor_item.find('.soda_anchor_img_txt_nl').html()!=''){
			soda_anchor_img_txt_nl[x] = anchor_item.find('.soda_anchor_img_txt_nl').html();
		}
		if(anchor_item.find('.soda_anchor_img_txt_en').length&&anchor_item.find('.soda_anchor_img_txt_en').html()!=''){
			soda_anchor_img_txt_en[x] = anchor_item.find('.soda_anchor_img_txt_en').html();
		}
		if(anchor_item.find('.soda_anchor_img_txt_nl_2').length&&anchor_item.find('.soda_anchor_img_txt_nl_2').html()!=''){
			soda_anchor_img_txt_nl_2[x] = anchor_item.find('.soda_anchor_img_txt_nl_2').html();
		}
		if(anchor_item.find('.soda_anchor_img_txt_en_2').length&&anchor_item.find('.soda_anchor_img_txt_en_2').html()!=''){
			soda_anchor_img_txt_en_2[x] = anchor_item.find('.soda_anchor_img_txt_en_2').html();
		}
					
		if(anchor_item.find('.soda_anchor_video').length&&anchor_item.find('.soda_anchor_video').val()!=''){
			soda_anchor_video[x] = anchor_item.find('.soda_anchor_video').val();
		}
		if(anchor_item.find('.soda_anchor_video_embed').length&&anchor_item.find('.soda_anchor_video_embed').val()!=''){
			soda_anchor_video_embed[x] = anchor_item.find('.soda_anchor_video_embed').val();
		}
		if(anchor_item.find('.soda_anchor_video_img').length&&anchor_item.find('.soda_anchor_video_img').val()!=''){
			soda_anchor_video_img[x] = anchor_item.find('.soda_anchor_video_img').val();
		}
		if(anchor_item.find('.soda_anchor_video_txt_nl').length&&anchor_item.find('.soda_anchor_video_txt_nl').html()!=''){
			soda_anchor_video_txt_nl[x] = anchor_item.find('.soda_anchor_video_txt_nl').html();
		}
		if(anchor_item.find('.soda_anchor_video_txt_en').length&&anchor_item.find('.soda_anchor_video_txt_en').html()!=''){
			soda_anchor_video_txt_en[x] = anchor_item.find('.soda_anchor_video_txt_en').html();
		}
		
		if(anchor_item.find('.soda_anchor_order').length&&anchor_item.find('.soda_anchor_order').val()!=''){
			soda_anchor_order[x] = anchor_item.find('.soda_anchor_order').val();
		}
			
	}
	
	data_container.push(soda_anchor_img);
	data_container.push(soda_anchor_img_2);
	data_container.push(soda_anchor_img_size);
	data_container.push(soda_anchor_img_align);
	data_container.push(soda_anchor_img_txt_type);
	data_container.push(soda_anchor_img_txt_align);
	data_container.push(soda_anchor_img_txt_nl);
	data_container.push(soda_anchor_img_txt_en);
	data_container.push(soda_anchor_img_txt_nl_2);
	data_container.push(soda_anchor_img_txt_en_2);
	data_container.push(soda_anchor_video);
	data_container.push(soda_anchor_video_embed);
	data_container.push(soda_anchor_video_img);
	data_container.push(soda_anchor_video_txt_nl);
	data_container.push(soda_anchor_video_txt_en);
	data_container.push(soda_anchor_order);
}

function openMediaManager(){
	if (mediaUploader) {
		mediaUploader.open();
		return;
	}
	mediaUploader = wp.media.frames.file_frame = wp.media({title: 'Choose image', multiple: false });

	mediaUploader.on('select', function() {
		attachment = mediaUploader.state().get('selection').toJSON();
		jQuery.each(attachment, function( index, value ){
			if(type=='left-column'){
				jQuery('.selected .admin-linker-kolom .'+dest).val(value['id']);
				jQuery('.selected .admin-linker-kolom .thumb-container').append('<img src="'+value['url']+'" class="admin-img-thumb dest-'+dest+'">');
			}else{
				jQuery('.selected .anchor-item.selected .anchor-'+type+' .'+dest).val(value['id']);
				jQuery('.anchor.selected .anchor-item.selected .anchor-'+type+' .thumb-container').append('<img src="'+value['url']+'" class="admin-img-thumb dest-'+dest+'">');
			}
		});
		mediaUploader.escape();
	});
	mediaUploader.open();
}

function setLang(){
	jQuery('#soda_current_language').val(jQuery('.qtranxs-lang-switch-wrap li.active').attr('lang'));
	if(jQuery('.qtranxs-lang-switch-wrap li.active').attr('lang')=='nl'){
		jQuery('.english').hide();
		jQuery('.dutch:not([aria-hidden="true"])').show();
	}else{
		jQuery('.english:not([aria-hidden="true"])').show();
		jQuery('.dutch').hide();
	}
	jQuery('.textarea.active').removeClass('active');
}

function clearContenteditable(){
	jQuery('.textarea.active').html('');
}

function pasteHtmlAtCaret(html) {
    var sel, range;
    if (window.getSelection) {
        // IE9 and non-IE
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();

            // Range.createContextualFragment() would be useful here but is
            // non-standard and not supported in all browsers (IE9, for one)
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ( (node = el.firstChild) ) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);
            
            // Preserve the selection
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    } else if (document.selection && document.selection.type != "Control") {
        // IE < 9
        document.selection.createRange().pasteHTML(html);
    }
}

jQuery(document).ready(function($){	
	
	$(document).on('click','.qtranxs-lang-switch',function(){
		setLang();
	})
	
	$(document).on('click', '.minify', function(e){
		$(this).parent().parent().find('.minifyable').slideToggle('fast', function(){
			if($(this).parent().parent().find('.minifyable').css('display')=='none'){
				e.stopPropagation();
				$(this).parent().removeClass('selected, maximized');
			}else{
				$(this).parent().addClass('maximized');
			}
		});
	})
	
	$(document).on('paste', '[contenteditable]', function (e) {
	    e.preventDefault();
	    document.execCommand('inserttext', false, prompt('Paste something.'));
	});
	
	$(document).on('click', '.anchor.selected .anchor-item.selected .upload-image-buttons input:button, .anchor.selected .anchor-item.selected .video-image input:button, .anchor.selected .left-column-images input:button', function(){
		dest = $(this).attr('data-dest');
		type = $(this).attr('data-type');
		openMediaManager();
	})
	
	$(document).on('click','.textarea',function(){
		$('.textarea.active').removeClass('active');
		$(this).addClass('active');
	})
	
	// *** ANCHORS *** //
	
	/*Sortable Area*/
	if($('.admin-anchors-container').length){
		$('.admin-anchors-container').sortable({
			items: '.anchor:not(.maximized)',
			cursor: 'move',
			containment: 'parent',
			placeholder: 'anchor-placeholder',
			cancel: ':input,button,.textarea'
		});
		$(".sortable").disableSelection();
	}
	
	$(document).on('click','.anchor',function(){
		$('.anchor.selected').not('.homepage.anchor.selected').removeClass('selected');
		$('.anchor-item.selected').not('.homepage .anchor-item.selected').removeClass('selected');
		$(this).addClass('selected');
	});
	
	$(document).on('click','.anchor-item',function(e){
		e.stopPropagation();
		$('.anchor-item.selected').not('.homepage .anchor-item.selected').removeClass('selected');
		$(this).addClass('selected');
		
		$('.anchor.selected').not('.homepage.anchor.selected').removeClass('selected');
		$(this).parent().parent().addClass('selected');
	});
	
	$(document).on('click','.selected .delete-anchor', function(e){
		var deleteButton = $(this);
		if(confirm('Are you sure you want to delete this anchor?')){
			e.stopPropagation();
			$.ajax({
				type: "POST",
				url: "/wp-content/themes/soda.nl/delete-anchor.php",
				data: {id:deleteButton.parent().parent().find('.soda_anchor_id').val()},
		        success: function(server_response) {
					if(server_response=='success'){
						deleteButton.parent().parent().remove();
						if($('.save-anchor').length==0){
							$('#publishing-action').show();
						}
					}else{
						deleteButton.parent().parent().addClass('error');
						console.log('Error 1: '+server_response); 
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					deleteButton.parent().parent().addClass('error');
		            console.log('Error 2: '+errorThrown);  
		        }
			});
		}
	});
	$(document).on('click','.selected .delete-anchor-item', function(e){
		var deleteButton = $(this);
		if(confirm('Are you sure you want to delete this anchor item?')){
			var id = $(this).parent().parent().parent().find('.soda_anchor_id').val();
			//deleteButton.parent().parent().parent().find('.soda_anchor_items_count').val(deleteButton.parent().parent().parent().find('.soda_anchor_items_count').val()-1);
			deleteButton.parent().hide().remove();
			$('.selected .anchor-item').each(function(index){
				$(this).attr('data-count',index);
			});
			//console.log(deleteButton.parent().parent().parent().find('.soda_anchor_items_count').val());
			prepDataForSaving();
			console.log(data_container);
			$.ajax({
				type: "POST",
				url: "/wp-content/themes/soda.nl/edit-anchor.php",
				data: {id:id,anchors:data_container},
		        success: function(server_response) {
					if($.isNumeric(server_response)){
						//IMPLEMENT: RELOAD ANCHOR
						//deleteButton.parent().hide().remove();
						//location.reload();
						console.log(server_response);
						alert('Save the entire page to complete deleting the item');
					}else{
						deleteButton.parent().addClass('error');
						console.log('Error 1: '+server_response); 
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					$('.admin-anchors-container .selected').addClass('error');
		            console.log('Error 2: '+errorThrown);  
		        }
			});
			/*e.stopPropagation();
			$.ajax({
				type: "POST",
				url: "/wp-content/themes/soda.nl/update-count.php",
				data: {id:deleteButton.parent().parent().parent().find('.soda_anchor_id').val(),count:deleteButton.parent().parent().parent().find('.soda_anchor_items_count').val()},
		        success: function(server_response) {
					if(server_response=='success'){
						deleteButton.parent().hide().remove();
					}else{
						deleteButton.parent().addClass('error');
						console.log('Error 1: '+server_response); 
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					deleteButton.parent().addClass('error');
		            console.log('Error 2: '+errorThrown);  
		        }
			});*/
		}
	});
	
	$(document).on('click','.anchor.selected .add-image',function(e){
		e.stopPropagation();
		var number = 0;
		if($('.admin-anchors-container .selected.anchor .anchor-item').length){
			number = $('.admin-anchors-container .selected.anchor .anchor-item').length;
		}
		$('.anchor.selected .anchor-item.selected').removeClass('selected');
		if($('.anchor.selected .imageplaceholder').length){
			$('#pieces-container .anchor-image').clone().insertBefore($('.anchor.selected .imageplaceholder')).removeClass('hide').wrap('<div class="anchor-item selected" data-count="'+number+'"></div>');
			$('.anchor.selected .anchor-item.selected').prepend('Order: <input type="text" size="2" class="soda_anchor_order" name="soda_anchor_order[]" value="0"><a class="delete delete-anchor-item"><span class="dashicons dashicons-no-alt"></span></a>');
			$('.anchor.selected .imageplaceholder').remove();
		}else{
			$('#pieces-container .anchor-image').clone().insertBefore($('.anchor.selected .anchor-item:first')).removeClass('hide').wrap('<div class="anchor-item selected" data-count="'+number+'"></div>');
			$('.anchor.selected .anchor-item.selected').prepend('Order: <input type="text" size="2" class="soda_anchor_order" name="soda_anchor_order[]" value="0"><a class="delete delete-anchor-item"><span class="dashicons dashicons-no-alt"></span></a>');
		}
		if($('.anchor.selected .videoplaceholder').length){
			$('.anchor.selected .anchor-item:first').before($('.anchor.selected .videoplaceholder'));
		}
	});
	$(document).on('click','.anchor.selected .remove_image',function(e){
		$(this).parent().find('input').val('');
		$(this).parent().find('.thumb-container').html('');
	});
	$(document).on('click','.anchor.selected .add-video',function(e){
		e.stopPropagation();
		var number = 0;
		if($('.admin-anchors-container .selected.anchor .anchor-item').length){
			number = $('.admin-anchors-container .selected.anchor .anchor-item').length;
		}
		$('.anchor.selected .anchor-item.selected').removeClass('selected');
		if($('.anchor.selected .videoplaceholder').length){
			$('#pieces-container .anchor-video').clone().insertBefore($('.anchor.selected .videoplaceholder')).removeClass('hide').wrap('<div class="anchor-item selected" data-count="'+number+'"></div>');
			$('.anchor.selected .anchor-item.selected').prepend('Order: <input type="text" size="2" class="soda_anchor_order" name="soda_anchor_order[]" value="0"><a class="delete delete-anchor-item"><span class="dashicons dashicons-no-alt"></span></a>');
			$('.anchor.selected .videoplaceholder').remove();
		}else{
			$('#pieces-container .anchor-video').clone().insertBefore($('.anchor.selected .anchor-item:first')).removeClass('hide').wrap('<div class="anchor-item selected" data-count="'+number+'"></div>');
			$('.anchor.selected .anchor-item.selected').prepend('Order: <input type="text" size="2" class="soda_anchor_order" name="soda_anchor_order[]" value="0"><a class="delete delete-anchor-item"><span class="dashicons dashicons-no-alt"></span></a>');
		}
		if($('.anchor.selected .imageplaceholder').length){
			$('.anchor.selected .anchor-item:first').before($('.anchor.selected .imageplaceholder'));
		}
	});
	
	$(document).on('change', '.anchor.selected .anchor-item.selected .image-size', function(){
		//console.log(this.value);
		if(this.value=='small'){
			$('.anchor.selected .anchor-item.selected .upload-image-2-button, .anchor.selected .anchor-item.selected .second-txt-field').fadeIn();
		}else{
			$('.anchor.selected .anchor-item.selected .upload-image-2-button, .anchor.selected .anchor-item.selected .second-txt-field').hide();
		}
	});
	
	$(document).on('change', '.anchor.selected .anchor-item.selected .text-type', function(){
		if(this.value=='slide'){
			$('.anchor.selected .anchor-item.selected .slider-button').fadeIn();
		}else{
			$('.anchor.selected .anchor-item.selected .slider-button').hide();
		}
	});
	
	$(document).on('change', '.anchor.selected .anchor-item.selected .video-type', function(){
		if(this.value=='linked'){
			$('.anchor.selected .anchor-item.selected .video-image').fadeIn();
			$('.anchor.selected .anchor-item.selected .video-embed').hide();
		}else if(this.value=='embedded'){
			$('.anchor.selected .anchor-item.selected .video-embed').fadeIn();
			$('.anchor.selected .anchor-item.selected .video-image').hide();
		}
	});
	
	/*if($('.convert-to-rich-textfield').length>0){
		$('.convert-to-rich-textfield').each(function(){
			tinyMCE.execCommand('mceAddEditor', false, $(this).attr('id'));
			if($(this).hasClass('dutch')){
				tinyMCE.DOM.addClass($('#'+$(this).attr('id')+'_ifr').parent().parent(), 'dutch');
			}else{
				tinyMCE.DOM.addClass($('#'+$(this).attr('id')+'_ifr').parent().parent(), 'english');
			}
		});
	}*/
	
	$('#add-anchor').on('click', function(){
		var temp_id = 'temp-id-'+$('.admin-anchors-container').length;
		$('.anchor.selected, .anchor-item.selected').removeClass('selected');
		$('.admin-anchors-container').append($('#admin-anchor-placeholder').clone().removeAttr('id').removeClass('hide').addClass(temp_id+' selected'));
		$('#publishing-action').hide();
		//$('html,body').animate({marginTop:$('.anchor.selected').position().top}, 500);
	})
	$(document).on('click', '.selected .save-anchor', function(){
		if($('.admin-anchors-container .selected.anchor .anchor-item').length==0){
			alert('You have to add an image or video to the anchor before saving');
			return;
		}
		prepDataForSaving();
		//console.log(data_container);
		$.ajax({
			type: "POST",
			url: "/wp-content/themes/soda.nl/save-anchor.php",
			data: {anchors:data_container},
	        success: function(server_response) {
				if($.isNumeric(server_response)){
					$('.admin-anchors-container .selected').removeClass().addClass('anchor selected').attr('id', 'admin-anchor-'+server_response);
					$('.anchor.selected .soda_anchor_id').val(server_response);
					$('.anchor.selected .anchor-header').prepend('Anchor '+server_response);
					$('.anchor.selected .save-anchor').attr('class','edit-anchor').val('Update anker');
					if($('.save-anchor').length<=1){
						$('#publishing-action').show();
					}
				}else{
					$('.admin-anchors-container .selected').addClass('error');
					console.log('Error 1: '+server_response); 
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$('.admin-anchors-container .selected').addClass('error');
	            console.log('Error 2: '+errorThrown);  
	        }
		});
	})
	$(document).on('click', '.selected .edit-anchor', function(){
		prepDataForSaving();
		//console.log(data_container);
		var id = $(this).parent().parent().find('.soda_anchor_id').val();
		$.ajax({
			type: "POST",
			url: "/wp-content/themes/soda.nl/edit-anchor.php",
			data: {id:id,anchors:data_container},
	        success: function(server_response) {
				if($.isNumeric(server_response)){
					//IMPLEMENT: RELOAD ANCHOR
					$('.admin-anchors-container .selected').removeClass().addClass('anchor selected');
				}else{
					$('.admin-anchors-container .selected').addClass('error');
					console.log('Error 1: '+server_response); 
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$('.admin-anchors-container .selected').addClass('error');
	            console.log('Error 2: '+errorThrown);  
	        }
		});
	})
	
	// *** LEFT COLUMN *** //
	
	$(document).on('click', '.selected .reset-linker-kolom', function(){
		var deleteButton = $(this);
		if(confirm('Are you sure you want to reset the left column?')){
			$.ajax({
				type: "POST",
				url: "/wp-content/themes/soda.nl/reset-left-column.php",
				data: {id:deleteButton.parent().parent().find('.soda_linker_kolom_anker_id').val()},
		        success: function(server_response) {
					if(server_response=='success'){
						deleteButton.parent().parent().find('.left-column-type').val('none');
						deleteButton.parent().parent().find('.left-column-images, .upload-image-buttons .input, .second-txt-field').hide();
						deleteButton.parent().parent().find('.soda_left_column_img, .soda_left_column_img_2, .soda_left_column_img_big').val('');
						deleteButton.parent().parent().find('.textarea').html('');
						deleteButton.parent().parent().find('.thumb-container').html('');
						//$('.selected .add-right-column').prop("disabled", false);
					}else{
						deleteButton.parent().parent().addClass('error');
						console.log('Error 1: '+server_response); 
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					deleteButton.parent().parent().addClass('error');
		            console.log('Error 2: '+errorThrown);  
		        }
			});
		}
	})
	
	$(document).on('click', '.anchor.selected .add-left-column', function(){
		$('.anchor.selected .admin-linker-kolom-container').html($('#admin-linker-kolom-placeholder .admin-linker-kolom').clone());
		$(this).prop("disabled",true);
	});
	
	$(document).on('click', '.selected .save-left-column', function(){
		var saveButton = $(this);
		$.ajax({
			type: "POST",
			url: "/wp-content/themes/soda.nl/save-left-column.php",
			data: {
				anchor_id:$('.selected .soda_anchor_id').val(),
				anchor_title:$('.selected .soda_anchor_title_nl').val(),
				soda_left_column_type:$('.selected .soda_left_column_type').val(),
				soda_left_column_img:$('.selected .soda_left_column_img').val(),
				soda_left_column_img_2:$('.selected .soda_left_column_img_2').val(),
				soda_left_column_txt_nl:$('.selected .soda_left_column_txt_nl').html(),
				soda_left_column_txt_en:$('.selected .soda_left_column_txt_en').html(),
				soda_left_column_txt_nl_2:$('.selected .soda_left_column_txt_nl_2').html(),
				soda_left_column_txt_en_2:$('.selected .soda_left_column_txt_en_2').html(),
				soda_left_column_img_big:$('.selected .soda_left_column_img_big').val()
			},
	        success: function(server_response) {
				if($.isNumeric(server_response)){
					saveButton.parent().find('.soda_linker_kolom_anker_id').val(server_response);
					$('.anchor.selected .save-left-column').attr('class','update-left-column').val('Update linker kolom');
				}else{
					$('.selected .admin-linker-kolom').addClass('error');
					console.log('Error 1: '+server_response); 
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				$('.selected .admin-linker-kolom').addClass('error');
	            console.log('Error 2: '+errorThrown);  
	        }
		});
	})
	$(document).on('change', '.selected .left-column-type', function(){
		$('.selected .left-column-images, .selected .left-column-text, .selected .left_column_img, .selected .left_column_img_2, .selected .second-txt-field, .selected .left_column_img_big').hide();
		if(this.value=='quote'){
			$('.selected .left-column-text').fadeIn();
		}else if(this.value=='image'){
			$('.selected .left-column-images, .selected .left_column_img, .selected .left-column-text').fadeIn();
		}else if(this.value=='small-image'){
			$('.selected .left-column-images, .selected .left_column_img, .selected .left_column_img_2, .selected .left-column-text, .selected .second-txt-field').fadeIn();
		}else if(this.value=='small-image-quote'){
			//WERE NOT DOING THIS YET
		}else if(this.value=='image-left-column'){
			$('.selected .left-column-images, .selected .left_column_img_big').fadeIn();
		}
	});
	$(document).on('click','.remove_image_linker_kolom',function(e){
		$(this).parent().find('input').val('');
		$(this).parent().find('.thumb-container').html('');
	});
	
	
	// *** POSTS *** //
	
	$('.soda_post_type').on('change', function(){
		$('.anchor-image,.anchor-video').hide();
		if(this.value=='image'){
			$('.anchor-image').fadeIn();
		}else if(this.value=='video'){
			$('.anchor-video').fadeIn();
		}
	});
	
		
});

jQuery(window).load(function() {
	jQuery('body').addClass('initial-lang-'+jQuery('.qtranxs-lang-switch-wrap li.active').attr('lang'));	
});