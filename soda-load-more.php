<?php 
	require_once('/sites/soda.nl/www/wp-config.php');

	$start = $_GET['start'];
	$all_ids = array();
	$args_pagination = array( 'posts_per_page' => 12, 'post_type' => 'post', 'post_status' => 'publish', 'paged='.$start );
	$postslist_pagination = new WP_Query( $args_pagination );
	if ( $postslist_pagination->have_posts() ) :
		while ( $postslist_pagination->have_posts() ) : $postslist_pagination->the_post();
			
			$lng = qtranxf_getLanguage();

			$soda_post_type = get_post_meta( get_the_ID(), 'soda_post_type', true );
			$soda_anchor_img = get_post_meta( get_the_ID(), 'soda_anchor_img', true );
			$soda_anchor_img_2 = get_post_meta( get_the_ID(), 'soda_anchor_img_2', true );
			$soda_anchor_img_size = get_post_meta( get_the_ID(), 'soda_anchor_img_size', true );
			$soda_anchor_img_align = get_post_meta( get_the_ID(), 'soda_anchor_img_align', true );
			$soda_anchor_img_txt_type = get_post_meta( get_the_ID(), 'soda_anchor_img_txt_type', true );
			$soda_anchor_img_txt_align = get_post_meta( get_the_ID(), 'soda_anchor_img_txt_align', true );
			$soda_anchor_img_txt = get_post_meta( get_the_ID(), 'soda_anchor_img_txt', true );
			$soda_anchor_img_txt_2 = get_post_meta( get_the_ID(), 'soda_anchor_img_txt_2', true );
			$soda_anchor_video = get_post_meta( get_the_ID(), 'soda_anchor_video', true );
			$soda_anchor_video_embed = get_post_meta( get_the_ID(), 'soda_anchor_video_embed', true );
			$soda_anchor_video_img = get_post_meta( get_the_ID(), 'soda_anchor_video_img', true );
			$soda_anchor_video_txt = get_post_meta( get_the_ID(), 'soda_anchor_video_txt', true );

			if($soda_post_type=='image'){ //=image
				if($soda_anchor_img_size=='small'){
					echo '<div class="img-container '.$soda_anchor_img_align.' small">';
					echo wp_get_attachment_image($soda_anchor_img, 'soda_small');
					echo wp_get_attachment_image($soda_anchor_img_2, 'soda_small');
					echo '<div class="image-txt '.$soda_anchor_img_txt_type.' small">';
					if(!empty($soda_anchor_img_txt)){
						echo '<div id="soda_anchor_img_txt_'.$soda_anchor_img.'">'.qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($soda_anchor_img_txt).'</div>';
					}
					if(!empty($soda_anchor_img_txt_2)){
						echo '<div id="soda_anchor_img_txt_'.$soda_anchor_img_2.'">'.qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($soda_anchor_img_txt_2).'</div>';
					}
					if($soda_anchor_img_size=='small'){
						echo '<br class="clearfix"></div></div>';
					}else{
						echo '<br class="clearfix"></div>';
					}
				}else{
					echo '<div class="img-container '.$soda_anchor_img_align.' '.$soda_anchor_img_size.'">';
					echo wp_get_attachment_image($soda_anchor_img, 'soda_'.$soda_anchor_img_size);
					echo '<div class="image-txt '.$soda_anchor_img_txt_type.' '.$soda_anchor_img_txt_align.' '.$soda_anchor_img_size[$x].'">';
					if(!empty($soda_anchor_img_txt)){
						$txt_current_lng = qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($soda_anchor_img_txt);
						echo '<div id="soda_anchor_img_txt_'.$soda_anchor_img.'">';
						if($soda_anchor_img_txt_type=='slide'){
							$txt = explode("[*|slidingpoint|*]", $txt_current_lng);
							echo $txt[0].'</div><a class="more-text">&gt;</a><div class="slide-text hide"><div>'.$txt[1].'<a class="less-text">&lt;</a></div>';
						}else{
							echo $txt_current_lng;
						}
						echo '</div>';
					}
					echo '</div></div>';
				}

			}else{ //=video
				if($soda_anchor_video=='embedded'){ //=embed
					echo '<div class="video-embed">'.$soda_anchor_video_embed.'</div>';
				}else{ //=linked
					echo wp_get_attachment_image($soda_anchor_video_img, 'soda_large');								
				}
				echo '<div class="image-txt">';
				if(!empty($soda_anchor_video_txt)){
					echo '<div id="soda_anchor_video_txt_'.$soda_anchor_video_img.'">'.qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($soda_anchor_video_txt).'</div>';
				}
				echo '</div>';
			}

			
			$current_post_id = get_the_ID();
			endwhile;  
			wp_reset_postdata();
	endif;
	echo '<p>';
	
	$all_ids = array();
	$args_pagination = array( 'posts_per_page' => -1, 'post_type' => 'post', 'post_status' => 'publish');
	$postslist_pagination = new WP_Query( $args_pagination );
	if ( $postslist_pagination->have_posts() ) :
		while ( $postslist_pagination->have_posts() ) : $postslist_pagination->the_post();
			$all_ids[] = get_the_ID();
		endwhile;  
		wp_reset_postdata();
	endif;

	$total_nr_items = count($all_ids);
	$key = array_search($current_post_id, $all_ids);

	echo '<a href="'.get_template_directory_uri().'/soda-load-more.php?start='.($key+1).'" class="loadmore';
	if($key+1>=$total_nr_items){
		echo ' hide';
	}
	echo '">'.qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage('[:nl]Meer Soda...[:en]More Soda...[:]').'</a>';
	echo '</p>';
?>

