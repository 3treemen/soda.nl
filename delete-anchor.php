<?php 
require_once('/sites/soda.nl/www/wp-config.php');

if ( current_user_can('publish_posts') ) {
		
	if(isset($_POST['id'])){
		
		$id = $_POST['id'];
		
		wp_delete_post( $id );
		
		echo 'success';

	}else{
		
		echo 'Invalid ID';
		
	}

}else{
	
	echo 'User is not allowed to do this!';
	
}
?>
