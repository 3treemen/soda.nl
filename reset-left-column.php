<?php 
require_once('/sites/soda.nl/www/wp-config.php');

if ( current_user_can('publish_posts') ) {
		
	if(isset($_POST['id'])){
			
		update_post_meta($_POST['id'], 'soda_left_column_type', 'none');
		update_post_meta($_POST['id'], 'soda_left_column_img', '');
		update_post_meta($_POST['id'], 'soda_left_column_img_2', '');
		update_post_meta($_POST['id'], 'soda_left_column_txt_nl', '');
		update_post_meta($_POST['id'], 'soda_left_column_txt_en', '');
		update_post_meta($_POST['id'], 'soda_left_column_txt_nl_2', '');
		update_post_meta($_POST['id'], 'soda_left_column_txt_en_2', '');
		update_post_meta($_POST['id'], 'soda_left_column_img_big', '');

		echo $_POST['id'];

	}else{
		
		echo 'Invalid ID';
		
	}

}else{
	
	echo 'User is not allowed to do this!';
	
}
?>
