<?php 
require_once('/sites/soda.nl/www/wp-config.php');

if ( current_user_can('publish_posts') ) {
		
	if(isset($_POST['anchor_id'])){
				
		$post_id = wp_insert_post(array (
		    'post_type' => 'linker_kolom',
		    'post_title' => 'Anchor title: '.$_POST['anchor_title']. ' & ID: '.$_POST['anchor_id'],
		    'post_content' => '',
		    'post_status' => 'publish',
		    'comment_status' => 'closed',
		    'ping_status' => 'closed',
		));

		if($post_id) {
			
			add_post_meta($post_id, 'soda_left_column_type', $_POST['soda_left_column_type']);
			add_post_meta($post_id, 'soda_left_column_img', $_POST['soda_left_column_img']);
			add_post_meta($post_id, 'soda_left_column_img_2', $_POST['soda_left_column_img_2']);
			add_post_meta($post_id, 'soda_left_column_txt_nl', $_POST['soda_left_column_txt_nl']);
			add_post_meta($post_id, 'soda_left_column_txt_en', $_POST['soda_left_column_txt_en']);
			add_post_meta($post_id, 'soda_left_column_txt_nl_2', $_POST['soda_left_column_txt_nl_2']);
			add_post_meta($post_id, 'soda_left_column_txt_en_2', $_POST['soda_left_column_txt_en_2']);
			add_post_meta($post_id, 'soda_left_column_img_big', $_POST['soda_left_column_img_big']);
			
			//update anchor
			update_post_meta($_POST['anchor_id'], 'soda_linker_kolom_id', $post_id);
			
		}else{
			
			echo 'Could not save post';
			
		}

		echo $post_id;

	}else{
		
		echo 'Invalid ID';
		
	}

}else{
	
	echo 'User is not allowed to do this!';
	
}
?>
