<?php get_header(); ?>

<!-- section -->
<section role="main">
	
	<?php /* The loop */
	while ( have_posts() ) : the_post();
	?>
	
		<div id="content">
			<?php the_content(); ?>
		</div>
	
	<?php endwhile; ?>
	
	<?php
	
	// left column
	$lng = qtranxf_getLanguage();
	$soda_linker_kolom_id = get_post_meta(get_the_ID(),'soda_linker_kolom_id',true);
	if(!empty($soda_linker_kolom_id)){
		
		$soda_left_column_type = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_type', true );
		$soda_left_column_img = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img', true );
		$soda_left_column_img_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img_2', true );
		$soda_left_column_txt_nl = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_nl', true );
		$soda_left_column_txt_en = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_en', true );
		$soda_left_column_txt_nl_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_nl_2', true );
		$soda_left_column_txt_en_2 = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_txt_en_2', true );
		$soda_left_column_img_big = get_post_meta( $soda_linker_kolom_id, 'soda_left_column_img_big', true );

		if($soda_left_column_type!='none'){

			echo '<div class="left-column '.$soda_left_column_type.' left-column-page">';

			if($soda_left_column_type=='quote'){
				if($lng=='en'){
					echo $soda_left_column_txt_en;
				}else{
					echo $soda_left_column_txt_nl;
				}
			}
			if($soda_left_column_type=='image'){
				echo wp_get_attachment_image($soda_left_column_img, 'medium');
				if($lng=='en'){
					echo '<div class="caption">'.$soda_left_column_txt_en.'</div>';
				}else{
					echo '<div class="caption">'.$soda_left_column_txt_nl.'</div>';
				}
			}
			if($soda_left_column_type=='small-image'){
				echo '<div class="img-container small">';
				echo wp_get_attachment_image($soda_left_column_img, 'soda_small');
				echo wp_get_attachment_image($soda_left_column_img_2, 'soda_small');
				echo '<div class="image-txt">';
				if($lng=='en'){
					if($soda_left_column_txt_en!='none'){
						echo '<div class="small small-img-text-left english">'.$soda_left_column_txt_en.'</div>';
					}
					if($soda_left_column_txt_en_2!='none'){
						echo '<div class="small small-img-text-right english">'.$soda_left_column_txt_en_2.'</div>';
					}
				}else{
					if($soda_left_column_txt_nl!='none'){
						echo '<div class="small small-img-left dutch">'.$soda_left_column_txt_nl.'</div>';
					}
					if($soda_left_column_txt_nl_2!='none'){
						echo '<div class="small small-img-text-right dutch">'.$soda_left_column_txt_nl_2.'</div>';
					}
				}
				echo '<br class="clearfix"></div></div>';
			}
			if($soda_left_column_type=='image-left-column'){
				echo '<div class="image-left-side">'.wp_get_attachment_image($soda_left_column_img_big, 'soda_leftcolumn').'</div>';
			}

			echo '</div>';
		}

	}
	
	?>
	
	<?php $queried_post = get_post(get_the_ID()); ?>
	<div class="horizontal-line <?php echo strtolower(qtranxf_use('nl',$queried_post->post_title)); ?>"></div>

</section>
<!-- /section -->

<?php get_footer(); ?>