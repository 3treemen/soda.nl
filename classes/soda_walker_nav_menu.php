<?php
class soda_walker_nav_menu extends Walker_Nav_Menu {
  
	// add main/sub classes to li's and links
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ){
	    global $wp_query;
		
		$class_names = $value = '';
  
	    // passed classes
	    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
	    $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
		
	    $output .= '<li id="nav-menu-item-'. $item->ID . '" class="' . $class_names . '">';
	  
		if($item->ID=='228'){
			$item_output = sprintf( '%1$s<a href="%2$sprojecten-tussenpagina/">%3$s%4$s%5$s</a>',
		        $args->before,
				$item->url,
		        $args->link_before,
		        apply_filters( 'the_title', $item->title, $item->ID ),
				$args->link_after
		    );
		}else if($item->ID=='227'){
			$item_output = sprintf( '%1$s<a href="%2$stussenpagina-soda/" class="plus_menu">%3$s%4$s%5$s</a>',
		        $args->before,
				$item->url,
		        $args->link_before,
		        apply_filters( 'the_title', $item->title, $item->ID ),
				$args->link_after
		    );
		}else if($item->ID=='226'){
			$item_output = sprintf( '%1$s<a data-href="%2$s" href="%2$svisie/" class="bureau_menu">%3$s%4$s%5$s</a>',
		        $args->before,
				$item->url,
		        $args->link_before,
		        apply_filters( 'the_title', $item->title, $item->ID ),
				$args->link_after
		    );
		}else{
			$item_output = sprintf( '%1$s<a href="%2$s">%3$s%4$s%5$s</a>',
		        $args->before,
				$item->url,
		        $args->link_before,
		        apply_filters( 'the_title', $item->title, $item->ID ),
				$args->link_after
		    );
		}
		
		//print_r($item);
		
		if($item->ID=='228'){
			$categories = get_terms('category', array(
			 	'post_type' => array('projecten'),
			 	'fields' => 'all',
				'hide_empty' => true
			));
			$item_output .= '<ul class="submenu level-1 categories">';
			foreach($categories as $cat){
				$cats = get_the_category();
				$item_class = '';
				foreach( $cats as $c ){
					if($c->name==$cat->name){
						$item_class = 'active';
					}
				}
				$item_class .= ' '.$cat->slug;
				$item_output .= '<li class="'.$item_class.'"><a href="/projecten/'.$cat->slug.'">'.qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($cat->name).'</a>';
				$args = array( 'post_type' => 'projecten', 'cat' => $cat->term_id, 'post_status' => 'publish', 'posts_per_page'   => -1 );
				$sub_submenu = get_posts( $args );
				//if(have_posts($sub_submenu)){
					$item_output .= '<ul class="submenu level-2 projects">';
					foreach($sub_submenu as $project){
						if(has_category( $cat->term_id, $project->ID )){
							if($project->ID==get_the_ID()){
								$item_output .= '<li class="active">';
							}else{
								$item_output .= '<li>';
							}
							$item_output .= '<a href="'.get_permalink($project->ID).'">'.qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($project->post_title).'</a>';
							$anchors = get_post_meta( $project->ID, 'soda_anchor_id', true );
							if($anchors){
								$item_output .= '<ul class="submenu level-3 anchors hide">';
								foreach($anchors as $anchor){
									if(get_the_title($anchor)!=''){
										$item_output .= '<li><a href="#'.sanitize_title(get_the_title($anchor)).'" class="animate-scroll">'.get_the_title($anchor).'</a></li>';
									}
								}
								$item_output .= '</ul>';
							}
							$item_output .= '</li>';
						}
					}
					$item_output .= '</ul>';
				//}
				$item_output .= '</li>';
		     }
			$item_output .= '</ul>';
		}else if($item->ID=='227'){
			$args = array( 'post_type' => 'plus', 'post_status' => 'publish' );
			$sub_submenu = get_posts( $args );
			//if(have_posts($sub_submenu)){
				$item_output .= '<ul class="submenu level-1 plus">';
				foreach($sub_submenu as $plus){
					if($plus->ID==get_the_ID()){
						$item_output .= '<li class="active">';
					}else{
						$item_output .= '<li>';
					}
					$item_output .= '<a href="'.get_permalink($plus->ID).'">'.qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($plus->post_title).'</a>';
					$anchors = get_post_meta( $plus->ID, 'soda_anchor_id', true );
					if($anchors){
						$item_output .= '<ul class="submenu level-2 anchors hide">';
						foreach($anchors as $anchor){
							if(get_the_title($anchor)!=''){
								$item_output .= '<li><a href="#'.sanitize_title(get_the_title($anchor)).'" class="animate-scroll">'.get_the_title($anchor).'</a></li>';
							}
						}
						$item_output .= '</ul>';
					}
					$item_output .= '</li>';
				}
				$item_output .= '</ul>';
			//}
		}else if($item->ID=='226'){
			$args = array( 'post_type' => 'bureau', 'post_status' => 'publish' );
			$sub_submenu = get_posts( $args );
			//if(have_posts($sub_submenu)){
				$item_output .= '<ul class="submenu level-1 bureau">';
				foreach($sub_submenu as $bureau){
					if($bureau->ID==get_the_ID()){
						$item_output .= '<li class="active">';
					}else{
						$item_output .= '<li>';
					}
					$item_output .= '<a href="'.get_permalink($bureau->ID).'">'.qtranxf_useCurrentLanguageIfNotFoundUseDefaultLanguage($bureau->post_title).'</a>';
					$anchors = get_post_meta( $bureau->ID, 'soda_anchor_id', true );
					if($anchors){
						$item_output .= '<ul class="submenu level-2 anchors hide">';
						foreach($anchors as $anchor){
							/*if($bureau->ID==get_the_ID()){
								$item_output .= '<li class="active"><a href="#'.sanitize_title(get_the_title($anchor)).'" class="animate-scroll">'.get_the_title($anchor).'</a></li>';
							}else{*/
								if(get_the_title($anchor)!=''){
									$item_output .= '<li><a href="#'.sanitize_title(get_the_title($anchor)).'" class="animate-scroll">'.get_the_title($anchor).'</a></li>';
								}
							//}
						}
						$item_output .= '</ul>';
					}
					$item_output .= '</li>';
				}
			//}	
			$item_output .= '</ul>';			
		}
  
	    // build html
	    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $args );
	}
	
}
?>